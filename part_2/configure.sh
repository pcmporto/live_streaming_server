#!/bin/bash

#Crie pastas para acesso fácil
#Create folders for easy access
mkdir -p /var/nginx/www/hls/p360
mkdir -p /var/nginx/www/hls/p720
mkdir -p /var/nginx/scripts

cp master.m3u8 /var/nginx/www/hls/master.m3u8
cp index.html /var/nginx/www/index.html
cp nginx.conf /usr/local/nginx/conf/nginx.conf
cp free.sh /var/nginx/scripts/free.sh
cp live.sh /var/nginx/scripts/live.sh

sh install-ffmpeg.sh

#De ao usuário nginx o controle das novas pastas
#Change new folders owner to nginx
chown nginx:nginx -R /var/nginx/*