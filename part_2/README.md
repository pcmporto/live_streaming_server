# Private Live Streaming Server PART 2 #

This is a guide to help you create multiple resolutions output.\

## Get Started ##

### Installing and configuring dependencies ###
* Use chmod +x configure.sh install-ffmpeg.sh free.sh live.sh to make this files executable
* As root, do: 
* Run configure.sh
* Execute systemctl reload nginx.service

### Using it ###
* use rtmp://YOUR_SERVER_ADDRESS:1935/stream/{SOME_KEY} <- it is your key if you using OBS
* open http://YOUR_SERVER_ADDRESS:8080 and enjoy :D

Click [here](https://dev.to/cesarpaulomp/crie-seu-servidor-de-live-streaming-privado-part-2-65o) to see my article for more information.