# Private Live Streaming Server PART 3 #

in this part, when starting a stream, we create a master.m3u8 and an index.html for it.

## Get Started ##

### Installing and configuring dependencies ###
* Use chmod +x configure.sh
* As root, do: 
* Run configure.sh
* Execute systemctl reload nginx.service

### Using it ###
* use rtmp://YOUR_SERVER_ADDRESS:1935/stream/{SOME_KEY} <- it is your key if you using OBS
* open http://YOUR_SERVER_ADDRESS:8080/{SOME_KEY} and enjoy :D

Click [here](https://youtu.be/pWU4hBVnpNY) to see a video for more information.