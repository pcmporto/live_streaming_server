#!/bin/bash

#Versão do nginx
#Nginx version
NGINX_VERSION="1.17.10"

#Instale compilador e outras dependencias para compilar e instalar o nginx
#Install compilers and other dependencies to build and install nginx
apt-get install -y build-essential git tree \
    zlib1g-dev libssl-dev unzip libpcre3 libpcre3-dev

cd /tmp

#Baixe o código fonte do nginx e o módulo de rtmp
#Download nginx and RTMP module
wget https://github.com/defanator/nginx-rtmp-module/archive/fix-build-with-gcc8.zip
wget https://nginx.org/download/nginx-1.17.10.tar.gz
#descompacte arquivos baixados
#unzip downloaded files
tar -zxvf nginx-1.17.10.tar.gz && unzip fix-build-with-gcc8.zip

cd nginx-1.17.10

#Execute Antes de compilar e instalar execute ./configure para adicionar o módulo RTMP
#Run ./configure before build and install nginx to add rtmp module
./configure  --add-module=../nginx-rtmp-module-fix-build-with-gcc8
make && make install

#Crie um arquivo para cadastrar o Nginx como um serviço no Ubuntu. 
#Este cadastro nos facilita iniciar/terminar o Nginx com mais facilidade.
#Create nginx service in ubuntu
printf \
"[Unit]\n\
Description=nginx - high performance web server\n\
Documentation=https://nginx.org/en/docs/ \n\n\
[Service]\n\
Type=forking\n\
ExecStart=/usr/local/nginx/sbin/nginx\n\
ExecReload=/usr/local/nginx/sbin/nginx -s reload\n\
ExecStop=/usr/local/nginx/sbin/nginx -s stop\n\n\
[Install]\n\
WantedBy=multi-user.target" >> /etc/systemd/system/nginx.service