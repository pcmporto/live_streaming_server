#Diz ao nginx qual usuário do SO ele deve utilizar
user nginx nginx;
#Aqui removemos alguns limites para que mais pessoas
#acessem o servidor para distribuir ou assistir vídeos.
worker_processes auto;
worker_rlimit_nofile 100000;

#Define o diretório/nome do arquivo para os logs de erro.
error_log  /var/nginx/logs/error.log;

events {
    #Diz quantas pessoas simultâneas podem acessar
    worker_connections 4000;
}

rtmp {
    server {
        #Porta padrão para RTMP
        listen 1935;
        #Verifica se há conexão ativa com o cliente
        #Caso o cliente não responda o nginx fecha a conexão
        ping 30s;
        ping_timeout 30s;
        
        #Cria a rota para estabelecer conexões RTMP.
        #Basta configurar em seu programa de streaming
        #favorito a rota 
        #rtmp://SERVER_ADDRESS:1935/stream/{SOME_CODE}
        #para começar a transmitir
        application stream {
            live on;
            #Grava toda a live em um arquivo flv
            #no diretório especificado
            record all;
            record_path /var/nginx/rec;
            record_suffix -%d-%b-%y-%T.flv;
            record_unique off;

            #Liga distribuição do vídeo como HLS
            #A especificação pede para utilizar o tmp
            hls on;
            hls_path /tmp/hls;
            #Tamanho máximo de cada fragmento do vídeo
            hls_fragment 5s;
        }
    }
}

#Configura a distribuição
http {
    default_type application/octet-stream;

    server {
        #Porta HTTP padrão.
        listen 80 default_server;
        
        #Rota para acesso ao HLS
        location /hls {
            types {
                application/vnd.apple.mpegurl m3u8;
                video/mp2t ts;
            }
            add_header Cache-Control no-cache;
            root /tmp;
        }
    }
}