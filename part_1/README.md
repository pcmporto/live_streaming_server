# Private Live Streaming Server PART 1 #

This is a guide to help you create your own live private server using ubuntu.

## Get Started ##

### Installing and configuring dependencies ###
* Use chmod +x configure.sh install-nginx.sh to make this files executable
* As root, do: 
* Run configure.sh
* Execute systemctl start nginx.service
* Execute systemctl enable nginx.service

### Using it ###
* use rtmp://YOUR_SERVER_ADDRESS:1935/stream/{SOME_KEY} <- it is your key if you using OBS
* open http://YOUR_SERVER_ADDRESS/hls/{SOME_KEY}.m3u8 and enjoy :D

Click [here](https://dev.to/cesarpaulomp/crie-seu-servidor-de-live-privado-part1-1g2g) to see my article for more information.