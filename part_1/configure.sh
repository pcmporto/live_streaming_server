#!/bin/bash

#Atualize o sistema operacional
#Update operacional system
apt update -y && sudo apt upgrade -y

#Crie pastas para acesso fácil
#Create folders for easy access
mkdir -p /var/nginx/rec
mkdir -p /var/nginx/logs

#Crie um usuário para o nginx (melhora a segurança do sistema)
#Create an user for nginx app (improve security)
adduser nginx --system --no-create-home --group --disabled-login --disabled-password

#De ao usuário nginx o controle das novas pastas
#Change new folders owner to nginx
chown nginx:nginx -R /var/nginx/*

#Baixa e instala todos os pacotes necessários rodar o nginx
#Download and install all packages needed to run nginx
sh install-nginx.sh

#substitui o nginx conf padrao pelo nosso
#replace the default nginx conf with our
cp nginx.conf /usr/local/nginx/conf/nginx.conf